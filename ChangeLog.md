# Revision history for tex-builder

0.1.1.1
---
* Changes to README
* Update this changelog

0.1.1.0
---
* Fix bug which caused latexmk not to be used with xelatex.
* Improvements to warnings / error messages.

0.1.0.1
---
* Initial release.
